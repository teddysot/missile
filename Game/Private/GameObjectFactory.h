#pragma once

#include "GameObject.h"

#include "Circle.h"
#include "Explosion.h"
#include "Rectangle.h"
#include "Missile.h"

#include <vector>

class exEngineInterface;

class GameObjectFactory
{
public:
	GameObjectFactory();
	~GameObjectFactory();

	void Initialize(exEngineInterface * engine);

	// Draw Game Objects
	Circle* DrawCircle(exVector2 position, exColor color, float radius, exVector2 velocity);
	Rectangle* DrawRectangle(exVector2 position, exColor color, exVector2 size, exVector2 velocity);
	Missile* DrawMissile(exVector2 startPos, exVector2 endPos, exColor color);
	Explosion* DrawExplosion(exVector2 position, exColor color);

private:
	exEngineInterface* mEngine;
};