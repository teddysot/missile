#pragma once

#include "Engine/Public/EngineTypes.h"

class Circle;
class exEngineInterface;

class Score
{
public:
	Score();
	~Score() {}

	void Initialize(exEngineInterface * engine, Circle * circle);
	void Update(float deltaTime);
	void Draw();


private:
	int CheckPoint();

	exEngineInterface * mEngine;
	Circle * mCircle;

	int p1Score;
	int p2Score;

	int mFontID;

	exVector2 mTextP1Position;
	exVector2 mTextP2Position;
};