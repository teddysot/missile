#include "Explosion.h"
#include "Math.h"

#include "Engine/Public/EngineInterface.h"

void Explosion::Draw()
{
	if (mDoneExploded)
	{
		return;
	}

	mEngine->DrawCircle(mPosition, mRadius, { 255,255,0,255 }, 0);
}

void Explosion::Update(float fDeltaTime)
{
	mRadius = Math::Lerp(mRadius, 30.0f, fDeltaTime);
	

	if (mRadius >= 29.0f)
	{
		mDoneExploded = true;
	}
}

bool Explosion::IsBoundariesCollision()
{
	return false;
}

bool Explosion::IsDoneExploded()
{
	return mDoneExploded;
}

exVector2 Explosion::GetPosition()
{
	return exVector2();
}

void Explosion::ClearVelocity()
{
}
