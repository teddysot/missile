#include "Rectangle.h"
#include "Math.h"

#include "Engine/Public/EngineInterface.h"

void Rectangle::Draw()
{
	SetSize(mSize);
	mEngine->DrawBox(mStartPoint, mEndPoint, mColor, 0);
}

void Rectangle::Update(float fDeltaTime)
{
	exVector2 velocityToAdd = Math::VectorMultiply(mVelocity, fDeltaTime);

	// Collide Boundaries?
	bool bHasCollided = IsBoundariesCollision();

	if (bHasCollided)
	{
		return;
	}
	
	mPosition = Math::VectorAdd(mPosition, velocityToAdd);
}

bool Rectangle::IsBoundariesCollision()
{
	// Left
	if (mStartPoint.x <= 1.0f && mVelocity.x <= 0.0f && mVelocity.y == 0.0f)
	{
		return true;
	}
	// Top
	else if (mStartPoint.y <= 1.0f && mVelocity.y <= 0.0f && mVelocity.x == 0.0f)
	{
		return true;
	}
	// Right
	else if (mEndPoint.x >= kViewportWidth - 1.0f && mVelocity.x >= 0.0f && mVelocity.y == 0.0f)
	{
		return true;
	}
	// Bottom
	else if (mEndPoint.y >= kViewportHeight - 1.0f && mVelocity.y >= 0.0f && mVelocity.x == 0.0f)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Rectangle::SetSize(exVector2 size)
{
	mSize = size;
	mStartPoint = { mPosition.x - (mSize.x / 2), mPosition.y - (mSize.y / 2) };
	mEndPoint = { mPosition.x + (mSize.x / 2),  mPosition.y + (mSize.y / 2) };
}

exVector2 Rectangle::GetSize()
{
	return mSize;
}

void Rectangle::SetVelocity(exVector2 velocity)
{
	mVelocity = velocity;

	// Max Velocity
	if (mVelocity.y > 300.0f)
	{
		mVelocity.y = 300.0f;
	}

	// Min Velocity
	if (mVelocity.y < -300.0f)
	{
		mVelocity.y = -300.0f;
	}
}

exVector2 Rectangle::GetVelocity()
{
	return mVelocity;
}

exVector2 Rectangle::GetPosition()
{
	return mPosition;
}

exVector2 Rectangle::GetStartPoint()
{
	return mStartPoint;
}

exVector2 Rectangle::GetEndPoint()
{
	return mEndPoint;
}

void Rectangle::ClearVelocity()
{
	mVelocity = { 0.0f, 0.0f };
}
