#pragma once
#include "GameObject.h"

class Explosion : public GameObject
{
public:
	virtual void Draw() override;
	virtual void Update(float fDeltaTime) override;
	virtual bool IsBoundariesCollision() override;

	bool IsDoneExploded();

	// Position
	exVector2 GetPosition();

	// Reset Velocity
	void ClearVelocity();

private:
	exVector2 mSize;
	exVector2 mVelocity;

	float mRadius = 1.0f;

	bool mDoneExploded = false;
};