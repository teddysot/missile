#pragma once
#include "GameObject.h"

class Rectangle : public GameObject
{
public:
	Rectangle() {}

	virtual void Draw() override;
	virtual void Update(float fDeltaTime) override;
	virtual bool IsBoundariesCollision() override;

	// Size
	void SetSize(exVector2 size);
	exVector2 GetSize();

	// Velocity
	void SetVelocity(exVector2 velocity);
	exVector2 GetVelocity();
	
	// Position
	exVector2 GetPosition();
	exVector2 GetStartPoint();
	exVector2 GetEndPoint();

	// Reset Velocity
	void ClearVelocity();

private:
	exVector2 mStartPoint;
	exVector2 mEndPoint;

	exVector2 mSize;
	exVector2 mVelocity;
};