#pragma once
#include "GameObject.h"

#include "Rectangle.h"

#include <vector>

class Circle : public GameObject
{
public:
	Circle() { }

	virtual void Draw() override;
	virtual void Update(float fDeltaTime) override;
	virtual bool IsBoundariesCollision() override;

	// Check Paddle Collision
	bool IsPaddleCollision();

	// Set Paddles that will collide
	void SetPaddles(Rectangle* p1, Rectangle* p2);

	// Position
	void SetPosition(exVector2 position);
	exVector2 GetPosition();

	// Radius
	void SetRadius(float radius);
	float GetRadius();

	// Velocity
	void SetVelocity(exVector2 velocity);
	exVector2 GetVelocity();

	// Reset Velocity
	void ClearVelocity();

private:
	float mRadius;
	exVector2 mVelocity;
	exVector2 mBoundaryNormal;

	Rectangle* mPaddle1;
	Rectangle* mPaddle2;
	exVector2 mPaddleNormal;
};