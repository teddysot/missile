#include "Missile.h"
#include "Math.h"

#include "Engine/Public/EngineInterface.h"

void Missile::Draw()
{
	if (mPosition.y <= mEndPoint.y + 5.0f && mStartPoint.y >= mEndPoint.y
		|| mPosition.y >= mEndPoint.y - 5.0f && mStartPoint.y <= mEndPoint.y)
	{
		mExploded = true;
		return;
	}
	mEngine->DrawBox(mBoxStart, mBoxEnd, mColor, 0);
	mEngine->DrawLine(mStartPoint, mPosition, mColor, 0);
}

void Missile::Update(float fDeltaTime)
{
	SetSize(mSize);
	mPosition = Math::Lerp(mPosition, mEndPoint, fDeltaTime);
}

bool Missile::IsBoundariesCollision()
{
	return false;
}

void Missile::SetSize(exVector2 size)
{
	mSize = size;
	mBoxStart = { mPosition.x - (mSize.x / 2), mPosition.y - (mSize.y / 2) };
	mBoxEnd = { mPosition.x + (mSize.x / 2),  mPosition.y + (mSize.y / 2) };
}

exVector2 Missile::GetSize()
{
	return mSize;
}

exVector2 Missile::GetPosition()
{
	return mPosition;
}

void Missile::SetStartPoint(exVector2 startPos)
{
	mStartPoint = startPos;
}

exVector2 Missile::GetStartPoint()
{
	return mStartPoint;
}

void Missile::SetEndPoint(exVector2 endPos)
{
	mEndPoint = endPos;
}

exVector2 Missile::GetEndPoint()
{
	return mEndPoint;
}

bool Missile::IsExploded()
{
	return mExploded;
}

void Missile::ClearVelocity()
{
	mVelocity = { 0.0f, 0.0f };
}
