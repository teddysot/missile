#pragma once
#include "GameObject.h"

class Missile : public GameObject
{
public:
	virtual void Draw() override;
	virtual void Update(float fDeltaTime) override;
	virtual bool IsBoundariesCollision() override;

	// Size
	void SetSize(exVector2 size);
	exVector2 GetSize();

	// Position
	exVector2 GetPosition();

	void SetStartPoint(exVector2 startPos);
	exVector2 GetStartPoint();

	void SetEndPoint(exVector2 endPos);
	exVector2 GetEndPoint();

	bool IsExploded();

	// Reset Velocity
	void ClearVelocity();

private:
	exVector2 mStartPoint;
	exVector2 mEndPoint;
	exVector2 mSize;
	exVector2 mVelocity;

	exVector2 mBoxStart;
	exVector2 mBoxEnd;

	bool mCollided = false;
	bool mExploded = false;
};