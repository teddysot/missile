#include "Circle.h"
#include "Math.h"

#include "Engine/Public/EngineInterface.h"

void Circle::Draw()
{
	mEngine->DrawCircle(mPosition, mRadius, mColor, 0);
}

void Circle::Update(float fDeltaTime)
{
	exVector2 velocityToAdd = Math::VectorMultiply(mVelocity, fDeltaTime);
	mPosition = Math::VectorAdd(mPosition, velocityToAdd);
}

bool Circle::IsBoundariesCollision()
{
	// Left
	if ((mPosition.x - mRadius) <= 0.0f)
	{
		mBoundaryNormal = { 1, 0 };
		return true;
	}
	// Top
	else if ((mPosition.y - mRadius) <= 0.0f)
	{
		mBoundaryNormal = { 0, 1 };
		return true;
	}
	// Right
	else if ((mPosition.x + mRadius) >= (kViewportWidth))
	{
		mBoundaryNormal = { -1, 0 };
		return true;
	}
	// Bottom
	else if ((mPosition.y + mRadius) >= (kViewportHeight))
	{
		mBoundaryNormal = { 0, -1 };
		return true;
	}
	else
	{
		return false;
	}
}

bool Circle::IsPaddleCollision()
{
	// Right
	if (mPaddle1->GetPosition().x + (mPaddle1->GetSize().x / 2) > (mPosition.x - mRadius)
		&& mPaddle1->GetPosition().x - (mPaddle1->GetSize().x / 2) < (mPosition.x + mRadius)
		&& mPaddle1->GetPosition().y + (mPaddle1->GetSize().y / 2) > (mPosition.y - mRadius)
		&& mPaddle1->GetPosition().y - (mPaddle1->GetSize().y / 2) < (mPosition.y + mRadius))
	{
		mPaddleNormal = { 1.0f, 0.0f };
		return true;
	}
	// Left
	else if (mPaddle2->GetPosition().x + (mPaddle2->GetSize().x / 2) > (mPosition.x - mRadius)
		&& mPaddle2->GetPosition().x - (mPaddle2->GetSize().x / 2) < (mPosition.x + mRadius)
		&& mPaddle2->GetPosition().y + (mPaddle2->GetSize().y / 2) > (mPosition.y - mRadius)
		&& mPaddle2->GetPosition().y - (mPaddle2->GetSize().y / 2) < (mPosition.y + mRadius))
	{
		mPaddleNormal = { 1.0f, 0.0f };
		return true;
	}
	else
	{
		return false;
	}
}

void Circle::SetPosition(exVector2 position)
{
	mPosition = position;
}

exVector2 Circle::GetPosition()
{
	return mPosition;
}

void Circle::SetPaddles(Rectangle * p1, Rectangle * p2)
{
	mPaddle1 = p1;
	mPaddle2 = p2;
}

void Circle::SetRadius(float radius)
{
	mRadius = radius;
}

float Circle::GetRadius()
{
	return mRadius;
}

void Circle::SetVelocity(exVector2 velocity)
{
	mVelocity = velocity;

	if (mVelocity.y > 300.0f)
	{
		mVelocity.y = 300.0f;
	}

	if (mVelocity.y < -300.0f)
	{
		mVelocity.y = -300.0f;
	}
}

exVector2 Circle::GetVelocity()
{
	return mVelocity;
}

void Circle::ClearVelocity()
{
	mVelocity = { 0.0f, 0.0f };
}
