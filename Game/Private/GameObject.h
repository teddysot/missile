#pragma once

#include "Engine/Public/EngineTypes.h"

class exEngineInterface;

class GameObject
{
public:
	virtual void Initialize(exEngineInterface * engine, exVector2 position, exColor color);
	virtual void Draw() = 0;
	virtual void Update(float fDeltaTime) = 0;
	virtual bool IsBoundariesCollision() = 0;

protected:
	exVector2 mPosition;
	exColor mColor;
	exEngineInterface* mEngine;
};