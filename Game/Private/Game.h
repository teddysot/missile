//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.h
// definition of MyGame, an implementation of exGameInterface
//

#include "Game/Public/GameInterface.h"
#include "Engine/Public/EngineTypes.h"
#include "Math.h"

#include "GameObjectFactory.h"
#include "Circle.h"
#include "Explosion.h"
#include "Rectangle.h"
#include "Missile.h"
#include "Score.h"

//-----------------------------------------------------------------
//-----------------------------------------------------------------

class MyGame : public exGameInterface
{
public:

								MyGame();
	virtual						~MyGame();

	virtual void				Initialize( exEngineInterface* pEngine );

	virtual const char*			GetWindowName() const;
	virtual void				GetClearColor( exColor& color ) const;

	virtual void				OnEvent( SDL_Event* pEvent );
	virtual void				OnEventsConsumed();

	virtual void				Run( float fDeltaT );

private:

	exEngineInterface*			mEngine;

	GameObjectFactory			mGameObjectFactory; // Game Object Factory

	std::vector<Circle*>		mSilos;
	std::vector<Rectangle*>		mBases;

	std::vector<Missile*>		mEnemyMissiles;
	std::vector<Explosion*>		mEnemyExplosions;

	Missile*					mMissile;
	Explosion*					mExplosion;
	
	// Score
	Score*						mScore;

	int							mFontID;

	bool						mEscape;

	bool						mLeftMouse;

	exVector2					mTextPosition;

	int mouseX;
	int mouseY;
};
