#include "Math.h"

#include <iostream>

float Math::Lerp(float firstFloat, float secondFloat, float by)
{
	return firstFloat * (1 - by) + secondFloat * by;
}

exVector2 Math::Lerp(exVector2 firstVector, exVector2 secondVector, float by)
{
	float retX = Lerp(firstVector.x, secondVector.x, by);
	float retY = Lerp(firstVector.y, secondVector.y, by);
	return { retX, retY };
}

float Math::Magnitude(exVector2 v)
{
	return sqrt((v.x * v.x) + (v.y * v.y));
}

float Math::Dot(exVector2 v1, exVector2 v2)
{
	return (v1.x * v2.x) + (v1.y * v2.y);
}

exVector2 Math::Normalized(exVector2 v)
{
	return { v.x / Magnitude(v), v.y / Magnitude(v) };
}

exVector2 Math::Cross(exVector2 v1, exVector2 v2)
{
	return { (v1.x * v2.y) , -(v1.y * v2.x) };
}

exVector2 Math::VectorAdd(exVector2 v1, exVector2 v2)
{
	exVector2 result;
	result.x = v1.x + v2.x;
	result.y = v1.y + v2.y;
	return result;
}

exVector2 Math::VectorSubtract(exVector2 v1, exVector2 v2)
{
	exVector2 result;
	result.x = v1.x - v2.x;
	result.y = v1.y - v2.y;
	return result;
}

exVector2 Math::VectorMultiply(exVector2 v1, float scalar)
{
	exVector2 result;
	result.x = v1.x * scalar;
	result.y = v1.y * scalar;

	return result;
}

exVector2 Math::VectorReflection(exVector2 v, exVector2 n)
{
	exVector2 result = VectorMultiply(n, 2 * Dot(v, n));
	result = VectorSubtract(v, result);
	return result;
}
