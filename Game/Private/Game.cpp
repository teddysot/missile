//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.cpp
// implementation of MyGame, an implementation of exGameInterface
//

#include "Game/Private/Game.h"

#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"

#include <stdio.h>

//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* gWindowName = "Teddy Pong Game";

//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::MyGame()
	: mEngine(nullptr)
	, mEscape(false)
	, mExplosion(nullptr)
	, mFontID(-1)
	, mLeftMouse(false)
	, mMissile(nullptr)
	, mScore(nullptr)
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::~MyGame()
{
	delete mScore;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::Initialize(exEngineInterface* pEngine)
{
	mEngine = pEngine;

	mFontID = mEngine->LoadFont("PSLSimilanyaRegular.ttf", 24);

	mTextPosition.x = 300.0f;
	mTextPosition.y = 50.0f;

	// Initialize Game Objects
	mGameObjectFactory.Initialize(mEngine);

	mSilos.push_back(mGameObjectFactory.DrawCircle({ 50.0f, 600.0f }, { 255,0,0,255 }, 50.0f, { 0.0f, 0.0f }));
	mSilos.push_back(mGameObjectFactory.DrawCircle({ 400.0f, 600.0f }, { 255,0,0,255 }, 50.0f, { 0.0f, 0.0f }));
	mSilos.push_back(mGameObjectFactory.DrawCircle({ 750.0f, 600.0f }, { 255,0,0,255 }, 50.0f, { 0.0f, 0.0f }));

	mBases.push_back(mGameObjectFactory.DrawRectangle({ 225.0f, 600.0f }, { 0,255,0,255 }, { 250.0f, 20.0f }, { 0.0f,0.0f }));
	mBases.push_back(mGameObjectFactory.DrawRectangle({ 575.0f, 600.0f }, { 0,255,0,255 }, { 250.0f, 20.0f }, { 0.0f,0.0f }));
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* MyGame::GetWindowName() const
{
	return gWindowName;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::GetClearColor(exColor& color) const
{
	color.mColor[0] = 128;
	color.mColor[1] = 128;
	color.mColor[2] = 128;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEvent(SDL_Event* pEvent)
{
	SDL_GetMouseState(&mouseX, &mouseY);
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEventsConsumed()
{
	int nKeys = 0;
	const Uint8 *pState = SDL_GetKeyboardState(&nKeys);
	const Uint8 pMState = SDL_GetMouseState(&mouseX, &mouseY);

	mLeftMouse = pMState & SDL_BUTTON_LMASK;

	mEscape = pState[SDL_SCANCODE_ESCAPE];
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::Run(float fDeltaT)
{
	exVector2 mousePos = { (float)mouseX, (float)mouseY };
	if (mEscape)
	{
		SDL_Quit();
	}

	for (int i = 0; i < mSilos.size(); i++)
	{
		mSilos[i]->Update(fDeltaT);
		mSilos[i]->Draw();
	}

	for (int i = 0; i < mBases.size(); i++)
	{
		mBases[i]->Update(fDeltaT);
		mBases[i]->Draw();
	}

	if (mLeftMouse)
	{
		exVector2 pos;
		if (mousePos.x >= 0 && mousePos.x <= 266.66f)
		{
			pos = { mSilos[0]->GetPosition().x, mSilos[0]->GetPosition().y - mSilos[0]->GetRadius() };
		}
		else if (mousePos.x >= 266.67f && mousePos.x <= 533.32f)
		{
			pos = { mSilos[1]->GetPosition().x, mSilos[1]->GetPosition().y - mSilos[1]->GetRadius() };

		}
		else if (mousePos.x >= 533.33f && mousePos.x <= 800.0f)
		{
			pos = { mSilos[2]->GetPosition().x, mSilos[2]->GetPosition().y - mSilos[2]->GetRadius() };
		}

		mMissile = mGameObjectFactory.DrawMissile(pos, mousePos, { 0,0,255,255 });
		mExplosion = mGameObjectFactory.DrawExplosion(mMissile->GetEndPoint(), { 255,255,0,255 });
	}

	if (mMissile != nullptr)
	{
		mMissile->Update(fDeltaT);
		mMissile->Draw();

		if (mMissile->IsExploded())
		{
			mExplosion->Update(fDeltaT);
			mExplosion->Draw();
		}
	}

	exVector2 enemyPos = { rand() % 800, 0 };

	exVector2 enemyTarget = { rand() % 800, rand() % 600 + 400 };

	mEnemyMissiles.push_back(mGameObjectFactory.DrawMissile(enemyPos, enemyTarget, { 0,0,255,255 }));

	for (int i = 0; i < mEnemyMissiles.size(); i++)
	{
		mEnemyMissiles[i]->Update(fDeltaT);
		mEnemyMissiles[i]->Draw();
		if (mEnemyMissiles[i]->IsExploded())
		{
			mEnemyExplosions.push_back(mGameObjectFactory.DrawExplosion(mEnemyMissiles[i]->GetEndPoint(), { 255,255,0,255 }));
		}
	}

	for (int j = 0; j < mEnemyExplosions.size(); j++)
	{
		mEnemyExplosions[j]->Update(fDeltaT);
		mEnemyExplosions[j]->Draw();

		if (mEnemyExplosions[j]->IsDoneExploded())
		{
			//mEnemyExplosions.erase(mEnemyExplosions.begin() + j);
			//mEnemyMissiles.erase(mEnemyMissiles.begin() + j);
		}
	}

	// Debug Ball
	char buffer[255];
	sprintf_s(buffer, sizeof(buffer), "Mouse X: %0.0fx", mousePos.x);
	exVector2 SizeText = { 350, mTextPosition.y };
	mEngine->DrawText(mFontID, SizeText, buffer, { 0, 255, 0 }, 1);

	sprintf_s(buffer, sizeof(buffer), "Mouse Y: %0.0fx", mousePos.y);
	SizeText = { 350, mTextPosition.y + 20.0f };
	mEngine->DrawText(mFontID, SizeText, buffer, { 0, 255, 0 }, 1);

	sprintf_s(buffer, sizeof(buffer), "Mouse Clicked: %d", mLeftMouse);
	SizeText = { 350, mTextPosition.y + 40.0f };
	mEngine->DrawText(mFontID, SizeText, buffer, { 0, 255, 0 }, 1);

	sprintf_s(buffer, sizeof(buffer), "Enemy Amount: %d", mEnemyMissiles.size());
	SizeText = { 350, mTextPosition.y + 60.0f };
	mEngine->DrawText(mFontID, SizeText, buffer, { 0, 255, 0 }, 1);

	if (mMissile != nullptr)
	{
		sprintf_s(buffer, sizeof(buffer), "Missile Pos: %0.2f, %0.2f", mEnemyMissiles[0]->GetPosition().x, mEnemyMissiles[0]->GetPosition().y);
		SizeText = { 350, mTextPosition.y + 80.0f };
		mEngine->DrawText(mFontID, SizeText, buffer, { 0, 255, 0 }, 1);

		sprintf_s(buffer, sizeof(buffer), "End Pos: %0.2f, %0.2f", mEnemyMissiles[0]->GetEndPoint().x, mEnemyMissiles[0]->GetEndPoint().y);
		SizeText = { 350, mTextPosition.y + 100.0f };
		mEngine->DrawText(mFontID, SizeText, buffer, { 0, 255, 0 }, 1);
	}
}
