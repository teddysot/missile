#pragma once
#include "Engine/Public/EngineTypes.h"

class Math
{
public:
	// https://stackoverflow.com/questions/33044848/c-sharp-lerping-from-position-to-position
	static float Lerp(float firstFloat, float secondFloat, float by);
	static exVector2 Lerp(exVector2 firstVector, exVector2 secondVector, float by);

	static float Magnitude(exVector2 v);
	static float Dot(exVector2 v1, exVector2 v2);

	static exVector2 Normalized(exVector2 v);
	static exVector2 Cross(exVector2 v1, exVector2 v2);

	static exVector2 VectorAdd(exVector2 v1, exVector2 v2);
	static exVector2 VectorSubtract(exVector2 v1, exVector2 v2);
	static exVector2 VectorMultiply(exVector2 v1, float scalar);
	static exVector2 VectorReflection(exVector2 v, exVector2 n);
};