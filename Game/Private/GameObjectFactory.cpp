#include "GameObjectFactory.h"

GameObjectFactory::GameObjectFactory()
	: mEngine(nullptr)
{
}

GameObjectFactory::~GameObjectFactory()
{
}

void GameObjectFactory::Initialize(exEngineInterface * engine)
{
	mEngine = engine;
}

Circle* GameObjectFactory::DrawCircle(exVector2 position, exColor color, float radius, exVector2 velocity)
{
	Circle* mCircle = new Circle();
	mCircle->Initialize(mEngine, position, color);
	mCircle->SetRadius(radius);
	mCircle->SetVelocity(velocity);

	return mCircle;
}

Rectangle* GameObjectFactory::DrawRectangle(exVector2 position, exColor color, exVector2 size, exVector2 velocity)
{
	Rectangle* mRectangle = new Rectangle();
	mRectangle->Initialize(mEngine, position, color);
	mRectangle->SetSize(size);
	mRectangle->SetVelocity(velocity);

	return mRectangle;
}

Missile* GameObjectFactory::DrawMissile(exVector2 startPos, exVector2 endPos, exColor color)
{
	Missile* mMissile = new Missile();
	mMissile->Initialize(mEngine, startPos, color);
	mMissile->SetSize({ 5.0f, 5.0f });
	mMissile->SetStartPoint(startPos);
	mMissile->SetEndPoint(endPos);

	return mMissile;
}

Explosion * GameObjectFactory::DrawExplosion(exVector2 position, exColor color)
{
	Explosion* mExplosion = new Explosion();
	mExplosion->Initialize(mEngine, position, color);

	return mExplosion;
}


