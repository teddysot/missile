#include "Score.h"

#include <stdio.h>

#include "Circle.h"

#include "Engine/Public/EngineInterface.h"

Score::Score()
	: mEngine(nullptr)
	, mCircle(nullptr)
	, p1Score(0)
	, p2Score(0)
{
}

void Score::Initialize(exEngineInterface * engine, Circle * circle)
{
	mEngine = engine;
	mCircle = circle;

	mTextP1Position = { 100.0f, 100.0f };
	mTextP2Position = { 600.0f, 100.0f };

	mFontID = mEngine->LoadFont("PSLSimilanyaRegular.ttf", 24);
}

void Score::Update(float deltaTime)
{
	switch (CheckPoint())
	{
	case 1:
		p1Score++;
		break;
	case 2:
		p2Score++;
		break;
	default:
		break;
	}
}

void Score::Draw()
{
	char buffer[255];

	sprintf_s(buffer, sizeof(buffer), "Player 1 : %d", p1Score);
	mEngine->DrawText(mFontID, mTextP1Position, buffer, { 255, 0, 0 }, 1);

	sprintf_s(buffer, sizeof(buffer), "Player 2 : %d", p2Score);
	mEngine->DrawText(mFontID, mTextP2Position, buffer, { 255, 0, 0 }, 1);
}

int Score::CheckPoint()
{
	// Left
	if ((mCircle->GetPosition().x - mCircle->GetRadius()) <= 0.0f)
	{
		return 2;
	}
	// Right
	else if ((mCircle->GetPosition().x + mCircle->GetRadius()) >= kViewportWidth)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
